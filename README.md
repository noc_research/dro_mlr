# DRO_MLR


Source code and Supplementary material for paper "Distributionally Robust Multiclass Classification and Applications in Deep Image Classifiers".

[Chen, Ruidi, Boran Hao, and Ioannis Ch Paschalidis. "Distributionally Robust Multiclass Classification and Applications in Deep Image Classifiers." ICASSP 2023-2023 IEEE International Conference on Acoustics, Speech and Signal Processing (ICASSP). IEEE, 2023.](https://ieeexplore.ieee.org/abstract/document/10095775?casa_token=32VYWBIxYA8AAAAA:NfOwVdimn_mq4ycUpKlO0CA2eeZlbg5XOK_PkTFXDJfPp7YY0RQpre3gS7w7eguUxcogpWZE)

Code here is for MNIST dataset under WGN, which can be replaced by different settings.

\
**Basic environment:** 

```
conda env create -f environment.yml
```


\
**MATLAB:**

MATLAB version R2022a; CVX v2.2; SDPT3 v4.0


\
**Supplement:**

`supplement.pdf`: Omitted proofs and extra experimental results.

